# Sesión 12-13
-------------------
#### Instalar dependencias y utilerías de webpack
```
$ npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin
```

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```

#### Intsalar dependencia Bulma
```
npm i -D bulma
```

#### Intsalar dependencia fontawesome
```
npm i -D @fortawesome/fontawesome-free
```

#### Algunos sitios de interes

  - [Bulma with node sass](https://bulma.io/documentation/customize/with-node-sass/)
  - [Bulma Colors](https://bulma.io/documentation/overview/colors/)
  - [Bulma Options](https://bulma.io/documentation/columns/options/)
  - [Webpack Plugin - Options](https://github.com/jantimon/html-webpack-plugin#options)
  - [Fontawesome](https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use)