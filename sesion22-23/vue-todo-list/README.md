# Sesión 22-23
-------------------
# vue-todo-list

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#### Algunos sitios de interes

  - [TodoMVC](http://todomvc.com/)
  - [Array-filter](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter)
  - [List rendering](https://v3.vuejs.org/guide/list.html#mapping-an-array-to-elements-with-v-for)
  - [Binding en Formularios-Input](https://es.vuejs.org/v2/guide/forms.html#Input)
  - [Binding en Formularios](https://es.vuejs.org/v2/guide/forms.html)
  - [Emitiendo un valor con un evento](https://es.vuejs.org/v2/guide/components.html#Emitiendo-un-valor-con-un-Evento)
  - [Referencia de eventos](https://developer.mozilla.org/es/docs/Web/Events)
  - [Computed Properties and Watchers](https://vuejs.org/v2/guide/computed.html)
  - [Computed vs Watched Property](https://vuejs.org/v2/guide/computed.html#Computed-vs-Watched-Property)
  - [JavaScript HTML DOM Events Examples](https://www.w3schools.com/js/js_events_examples.asp)
  - [Vue.js Enlace Clases y Estilos](https://es.vuejs.org/v2/guide/class-and-style.html)
  - [Hyper](https://hyper.is/)
  - [WebAssembly](https://webassembly.org/)
  - [WebAssembly - Developers guide](https://webassembly.org/getting-started/developers-guide/)
  - [State of JS 2020](https://2020.stateofjs.com/es-ES/)
  - [State of JS 2020: Front-end Frameworks](https://2020.stateofjs.com/es-ES/technologies/front-end-frameworks/)