# Sesión 19
-------------------
#### Instalar dependencia browser-sync
Añadir en el archivo `package.json`, en la parte de scripts lo siguiente:
```
"scripts": {
    "start": "npx browser-sync src -w"
  }
```
`NOTA:` `npx` es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, `-w` propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto

#### Instalar Vue por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://unpkg.com/vue/dist/vue.js"></script>
```

#### Instalar Vue Router por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
```

#### Instalar Vuex por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://unpkg.com/vuex/dist/vuex.js"></script>
```

#### Instalar Vue Devtools
```
npm install -g @vue/devtools
```
Otra manera de obtenerlo es:
[Descarga la extensión para Chrome](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

#### Algunos sitios de interes

  - [GitHub Vue Devtools](https://github.com/vuejs/vue-devtools)
  - [API Web History](https://developer.mozilla.org/es/docs/Web/API/History)
  - [Manipulando el historial del navegador](https://developer.mozilla.org/es/docs/DOM/Manipulando_el_historial_del_navegador)
  - [Diagrama ciclo de vida](https://es.vuejs.org/v2/guide/instance.html#Diagrama-del-Ciclo-de-vida)
  - [Best of JS - State management](https://bestofjs.org/projects?tags=state)
  - [Vuex](https://vuex.vuejs.org/)
  - [Vuex flow](https://vuex.vuejs.org/flow.png)
  - [Falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)