# Sesión 15
-------------------
#### Instalar dependencias y utilerías de webpack
```
$ npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin
```

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```

#### Instalar dependencia Bootstrap 5 (versión beta)
```
npm i -D bootstrap@next
```

#### Instalar dependencia fontawesome
```
npm i -D @fortawesome/fontawesome-free
```

#### Algunos sitios de interes

  - [CSS Especificidad](https://developer.mozilla.org/es/docs/Web/CSS/Especificidad)