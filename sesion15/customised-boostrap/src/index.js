import './sass/styles.scss'
import '@fortawesome/fontawesome-free/js/all'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Customised Bootstrap ***')
