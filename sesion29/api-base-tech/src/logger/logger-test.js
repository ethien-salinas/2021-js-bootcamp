require('dotenv').config()
const logger = require('./index')

logger.info('*** winston ***')

logger.error(`process.env.NODE_ENV: , ${process.env.NODE_ENV}`)

logger.debug(`process.env.JWT_SECRET: , ${process.env.JWT_SECRET}`)

logger.debug(`process.env.DB_HOST: , ${process.env.DB_HOST}`)
logger.debug(`process.env.DB_USER: , ${process.env.DB_USER}`)
logger.debug(`process.env.DB_PASS: , ${process.env.DB_PASS}`)
