# Sesión 29
-------------------
#### Instalar dependencia bcrypt
```
npm i bcrypt
```

#### Instalar dependencia dotenv
```
npm i dotenv
```

#### Instalar dependencia winston
```
npm i winston
```

#### Instalar dependencia winston-daily-rotate-file
```
npm i winston-daily-rotate-file
```

#### Algunos sitios de interes

  - [10 Cosas de las que me arrepiento de Node.js - Ryan Dahl](https://www.youtube.com/watch?v=M3BM9TB-8yA)
  - [10 Things I Regret About Node.js - Ryan Dahl](https://morioh.com/p/8f14b95d2a0e)
  - [The twelve-factor app](https://12factor.net/es/)
  - [Systemd](https://es.wikipedia.org/wiki/Systemd)
  - [DotEnv](https://www.npmjs.com/package/dotenv)
  - [GoDotEnv](https://github.com/joho/godotenv)
  - [DotEnv GitHub](https://github.com/bkeepers/dotenv)
  - [Winston](https://www.npmjs.com/package/winston)
  - [winston-daily-rotate-file](https://www.npmjs.com/package/winston-daily-rotate-file)
