# Sesión 36
-------------------
#### Instalar dependencias
```
npm i typeorm sqlite3 reflect-metadata graphql apollo-server apollo-datasource
```

#### Instalar dependencias de desarrollo
```
npm i -D typescript ts-node @types/node
```

#### Algunos sitios de interes

  - [Random Book Generator](https://www.datamocks.com/generator/books/)
  - [spreadsheets](https://docs.google.com/spreadsheets/d/1WF9yrkRSw3fqJac5bZxza0qxCjajZwYX-_waWbCb42g/edit?usp=sharing)