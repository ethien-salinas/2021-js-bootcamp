import { gql } from 'apollo-server'

export const Book = gql`
  type Book {
    id: Int
    title: String
    author: String
    publisher: String
    isbn10: String
    isbn13: String
    category: String
    year: Int
    language: String
    totalPages: Int
  }
  input BookInput {
    title: String
    author: String
    publisher: String
    isbn10: String
    isbn13: String
    category: String
    year: Int
    language: String
    totalPages: Int
  }
` 
