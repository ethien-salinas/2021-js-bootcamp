import { gql } from 'apollo-server'

export const Mutation = gql`

  type Mutation {
    saveBook(input: BookInput): Book
    updateBook(id: Int!, input: BookInput): Book
    deleteBook(id: Int!): Book
  }

`
