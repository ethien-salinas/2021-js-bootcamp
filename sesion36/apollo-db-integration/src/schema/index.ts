import { gql } from 'apollo-server'
import { Book } from './model/Book'
import { Mutation } from './root/Mutation'
import { Query } from './root/Query'

export const typeDefs = gql`
  ${Book}
  ${Query}
  ${Mutation}
`