import { ApolloServer } from 'apollo-server'
import { getDBConnection } from './db'
import { typeDefs } from './schema'
import { resolvers } from './resolver'
import { BookAPI } from './datasource/BookAPI'
import { Connection } from 'typeorm'

let connection: Connection
(async () => {
  connection = await getDBConnection()
})()

new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      booksAPI: new BookAPI(connection)
    }
  }
})
  .listen()
  .then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  })