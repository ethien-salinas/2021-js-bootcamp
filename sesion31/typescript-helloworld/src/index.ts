let helloWorld = "Hello World";

interface User {
  id: number;
  name: string;
  isAdmin: boolean;
}

const user: User = {
  id: 0,
  // username: "Hayes", // solo nodos que existan
  name: "Hayes",
  // is_admin: true,       // hay que homogenizar el uso de variables
  isAdmin: true
};
console.log(user);

class UserAccount {
  id: number;
  name: string;
  isAdmin: boolean;

  constructor(name: string, id: number, isAdmin: boolean) {
    this.id = id;
    this.name = name;
    this.isAdmin = isAdmin;
  }
}

const userAcc: User = new UserAccount("Murphy", 1, true);
console.log(userAcc);
