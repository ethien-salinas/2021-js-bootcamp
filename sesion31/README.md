# Sesión 31
-------------------
#### Instalar dependencia sqlite3
```
npm i sqlite3
```

#### Instalar dependencia typeorm y reflect-metadata
```
npm i typeorm reflect-metadata
```

#### Instalar dependencia typescript
```
npm i -D typescript
```

#### Instalar dependencia ts-node
```
npm i -D ts-node
```

#### Algunos sitios de interes

  - [typescriptlang](https://www.typescriptlang.org/docs)
  - [typescript in 5 minutes](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)
  - [typescript tooling in 5 minutes](https://www.typescriptlang.org/docs/handbook/typescript-tooling-in-5-minutes.html)
  - [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html)
  - [TypeORM](https://typeorm.io/#/)
