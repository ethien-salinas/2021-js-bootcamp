import { Connection } from "typeorm";
import { getDBConnection } from "./db";
import { UserAPI } from "./db/api/UserAPI";
import { IUser } from "./model/IUser";

let connection: Connection
let userAPI: UserAPI

(async () => {
    connection = await getDBConnection()
    userAPI = new UserAPI(connection)
    const josue: IUser = {
        name: "Josué",
        email: "josue.abenamar@mail.com",
        password: "qwerty",
        createdAt: new Date(),
        updatedAt: new Date(),
    };
    await userAPI.saveUser(josue)
})()

