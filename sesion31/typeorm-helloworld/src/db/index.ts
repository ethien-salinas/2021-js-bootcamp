import { createConnection } from "typeorm";
declare var __dirname

export const getDBConnection = async () => {
  const connection = await createConnection({
    type: 'sqlite',
    database: 'database.sqlite',
    synchronize: true,
    logging: true,
    entities: [
      `${__dirname}/entity/*`
    ]
  })
  return connection
}