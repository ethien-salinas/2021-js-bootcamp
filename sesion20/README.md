# Sesión 20
-------------------
#### Instalar Vue-cli por npm
```
npm install -g @vue/cli
```

#### Crear proyecto Vue por terminal
Con el comando `vue create` seguido del nombre del proyecto:
```
vue create my-first-vue-app
```

#### Crear proyecto Vue por interfaz gráfica
Con el comando `vue ui` ejecutamos la interfaz gráfica:
```
vue ui
```

#### Algunos sitios de interes

  - [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
  - [JavaScript Standard Style](https://standardjs.com/)
  - [JavaScript Standard Style - can you change it?
](https://standardjs.com/#i-disagree-with-rule-x-can-you-change-it)
  - [Prettier](https://prettier.io/)