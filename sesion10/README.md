# Sesión 10
-------------------

#### Instalar dependencias y utilerías de webpack
```
$ npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin
```

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```

#### Algunos sitios de interes

  - [Sass Color Lighten](https://sass-lang.com/documentation/modules/color#lighten)
  - [Sass Color Darken](https://sass-lang.com/documentation/modules/color#darken)
  - [Clip-Path](https://developer.mozilla.org/es/docs/Web/CSS/clip-path)
  - [Clippy](https://bennettfeely.com/clippy/)
  - [Fonts Google](https://fonts.google.com/)