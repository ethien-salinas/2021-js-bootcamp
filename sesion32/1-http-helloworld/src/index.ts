import http from 'http';

const hostname: string = '127.0.0.1';
const port: number = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  switch (req.url) {
    case '/':
      if (req.method === 'GET' || req.method === 'OPTIONS') {
        res.end(`[${req.method}][${req.url}]`);
      } else {
        res.statusCode = 405;
        res.end()
      }
      break
    case '/login':
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World');
      break
    case '/user':
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World');
      break
  }

  console.log(`req.url: ${req.url}`);
  console.log(`req.method: ${req.method}`);


});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});