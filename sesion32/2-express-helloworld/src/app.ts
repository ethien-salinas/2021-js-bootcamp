import express, { Request, Response } from "express"
import { userRouter } from "./router/user";

const app = express()
const port: number = 3000

app.use(express.json())

app.get('/', (req: Request, res: Response) => {
  res.send(`[${req.method}] Root path`)
})
app.options('/', (req: Request, res: Response) => {
  res.send(`[${req.method}] Root path`)
})
app.post('/login', (req: Request, res: Response) => {
  console.log(req.body)
  res.send(`[${req.method}] login, [${JSON.stringify(req.body)}]`)
})
// using router
app.use('/user', userRouter)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})