# Sesión 32
-------------------
## * Proyecto http-helloworld
#### Instalar dependencia typescript ts-node @types/node
```
npm i -D typescript ts-node @types/node
```

### * Proyecto express-helloworld
#### Instalar dependencia typeorm y reflect-metadata
```
npm i -D typescript ts-node @types/node @types/express
```

#### Instalar dependencia express
```
npm i express
```

#### Algunos sitios de interes

  - [Extensión VSCode rest-client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
  - [HTTP Methods](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
  - [HTTP Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
  - [HTTP Header](https://developer.mozilla.org/es/docs/Web/HTTP/Headers)
