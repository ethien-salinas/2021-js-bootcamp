# Sesión 21
-------------------
#### Instalar vue-router
```
npm i vue-router
```

#### Instalar vuex
```
npm i vuex
```

#### Algunos sitios de interes

  - [vue-router](https://www.npmjs.com/package/vue-router)
  - [Single Sign-On](https://es.wikipedia.org/wiki/Single_Sign-On)
  - [Double NOT (!!)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_NOT#double_not_!!)
  - [Promise](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Promise)
  - [Promise image](https://mdn.mozillademos.org/files/8633/promises.png)
  - [Callbacks vs Promise vs Async/Await en JavaScript](http://www.paulomogollon.com/promises-vs-async-await-vs-callbacks-en-javascript-espanol/)