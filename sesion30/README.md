# Sesión 30
-------------------
#### Instalar dependencia bcrypt
```
npm i bcrypt
```

#### Instalar dependencia dotenv
```
npm i dotenv
```

#### Instalar dependencia jsonwebtoken
```
npm i jsonwebtoken
```

#### Instalar dependencia sqlite3
```
npm i sqlite3
```

#### Instalar dependencia sequelize
```
npm i sequelize
```

#### Crear DB desde terminal en Mac o Linux
```
sqlite3 database.sqlite

CREATE TABLE user (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT,
email TEXT NOT NULL UNIQUE,
password TEXT NOT NULL,
created_at DATETIME,
updated_at DATETIME
);
```

#### Sintaxis IIFE: Expresión de función ejecutada inmediatamente
```
(() => { })() 
(async () => { })()
```

#### Algunos sitios de interes

  - [Extensión VSCode sqlite](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite)
  - [Sequelize](https://sequelize.org/master/manual/model-basics.html#data-types)
  - [IIFE: Expresión de función ejecutada inmediatamente](https://developer.mozilla.org/es/docs/Glossary/IIFE)
