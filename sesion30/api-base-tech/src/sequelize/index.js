const { Sequelize } = require('sequelize')
const bcrypt = require('bcrypt')

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite'
});

(async () => {
  try {
    // para probar la conexión a la BD
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');

    // importamos modelo
    const User = require('./entity/User')(sequelize, Sequelize.DataTypes)

    // *** Create a new user ***
    // const erick = await User.create({
    //   name: "Erick",
    //   email: "De la Paz Pérez",
    //   password: "qwerty"
    // });
    // console.log("Erick':", erick);

    // *** Read All (returns an arr) ***
    // console.log(await User.findAll());
    // *** Read One (returns an object) ***
    console.log(await User.findOne({
      where: {
        id: 2
      }
    }));

    // *** Update ***
    // console.log(await User.update({
    //   password: await bcrypt.hash('qwerty', 9)
    // }, {
    //   where: { // en verdad, siempre usen where
    //     id: 1
    //   }
    // }));

  } catch (err) {
    console.error('Unable to connect to the database:', err);
  }
})()



