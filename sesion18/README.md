# Sesión 18
-------------------
#### Instalar dependencia browser-sync
Añadir en el archivo `package.json`, en la parte de scripts lo siguiente:
```
"scripts": {
    "start": "npx browser-sync src -w"
  }
```
`NOTA:` `npx` es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, `-w` propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto

#### Instalar Vue 2 por CDN
Agregar el siguiente script en la etiqueta head del html:
```
 <script src="https://unpkg.com/vue@2.6.12/dist/vue.js"></script>
```

#### Instalar Vue 3 por CDN
Agregar el siguiente script en la etiqueta head del html:
```
 <script src="https://unpkg.com/vue@next"></script>
```

#### Algunos sitios de interes

  - [Vue.js](https://vuejs.org/)
  - [Vue3.js ](https://v3.vuejs.org/)