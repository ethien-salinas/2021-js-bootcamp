# Sesión 11
-------------------

#### Intsalar dependencia browser-sync
Añadir en el archivo `package.json`, en la parte de scripts lo siguiente:
```
"scripts": {
    "start": "npx browser-sync src -w"
  }
```
`NOTA:` `npx` es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, `-w` propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto

#### Algunos sitios de interes

  - [cdnjs](https://cdnjs.com/)
  - [cdnjs bulma.min.css](https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.1/css/bulma.min.css)
  - [cdnjs bulma.css](https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.1/css/bulma.css)
  - [Bootstrap 1.0.0](https://getbootstrap.com/1.0.0/)
  - [Bulma Container](https://bulma.io/documentation/layout/container/)
  - [Bulma Block](https://bulma.io/documentation/elements/block/)
  - [Bulma Content](https://bulma.io/documentation/elements/content/)
  - [Bulma Box](https://bulma.io/documentation/elements/box/)
  - [Mobile Design Patterns](https://pttrns.com/)
  - [UI Patterns](http://ui-patterns.com/)