# Sesión 34
-------------------
### * Proyecto graphql-helloworld
#### Instalar dependencia json-graphql-server
```
npm i json-graphql-server
```

### * Proyecto graphql-apollo-helloworld
#### Instalar dependencias
```
npm i apollo-server graphql
```

#### Instalar dependencias de desarrollo
```
npm i -D ts-node typescript
```

#### Algunos sitios de interes

  - [json-graphql-server](https://www.npmjs.com/package/json-graphql-server)
  - [json-server](https://github.com/typicode/json-server)
  - [Hasura GraphQL Engine](https://github.com/hasura/graphql-engine)
  - [Hasura](https://hasura.io/)
  - [Apollo Server](https://github.com/apollographql/apollo-server)
  - [Apollo Server Documentation](https://www.apollographql.com/docs/apollo-server/)
  - [GraphQL](https://graphql.org/)
  - [GraphQL Specification Versions](http://spec.graphql.org/)
  - [GraphQL - June 2018](http://spec.graphql.org/June2018/)
  - [JSON Schema](https://json-schema.org/)
