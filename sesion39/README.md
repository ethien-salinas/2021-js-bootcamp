verificar instalación de go
```
go version
```

iniciar un módulo
```
go mod init <nombre-del-módulo>
```

ejecutar programa
```
go run <main.go>
```

compilar programa
```
go build <main.go>
```
