package main

import (
	"fmt"
	"net/http"
)

func myHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello Ethien!\n")
}

func main() {
	port := ":3000"
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello World http server!\n")
	})
	http.HandleFunc("/ethien", myHandler)
	http.ListenAndServe(port, nil)
}
