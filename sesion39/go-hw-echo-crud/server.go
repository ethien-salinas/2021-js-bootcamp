package main

import (
	"go-hw-echo-crud/handler"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/", hello)

	userGpo := e.Group("/user")
	userGpo.POST("", handler.SaveUser)
	userGpo.GET("", handler.GetUser)
	userGpo.GET("/:id", handler.GetUser)
	userGpo.PUT("/:id", handler.UpdateUser)
	userGpo.DELETE("/:id", handler.DeleteUser)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
