package main

import (
	"fmt"
	"go-hello-world-quote/quotes"
)

func main() {
	/**
	* declaración de constante
	* const <identificador> <tipo>
	 */
	const ConstString = "Hello world Go!"
	fmt.Println(ConstString)
	/**
	* declaración de variable
	* var <identificador> <tipo>
	 */
	var result string
	result = quotes.GetQuote("concurrency")
	fmt.Println(result)
	result = quotes.GetQuote("glass")
	fmt.Println(result)
	result = quotes.GetQuote("go")
	fmt.Println(result)
	result = quotes.GetQuote("hello")
	fmt.Println(result)
	result = quotes.GetQuote("opt")
	fmt.Println(result)
}
