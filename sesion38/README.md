# Sesión 38
-------------------
## AWS
acceso a consola con cuenta en URL
```
https://<aws-acc-id>.signin.aws.amazon.com/console
```
## AWS IAM
https://aws.amazon.com/iam/

## AWS CLI
https://aws.amazon.com/cli/
```
aws --version
aws-cli/2.0.10 Python/3.7.4 Darwin/20.3.0 botocore/2.0.0dev14
```
Documentación CLI
https://docs.aws.amazon.com/cli/latest/reference/

```
aws [options] <command> <subcommand> [parameters]
```
archivos de configuración
```
cat ~/.aws/config
cat ~/.aws/credentials
```
copiar de s3 a s3
```
aws --profile <configured_profile> s3 cp s3://ethien-myfirst-bucket s3://ethien-cli-bucket --include "*.txt" --recursive
```
copiar de mi máquina a s3
```
aws --profile <configured_profile> s3 cp ./html5up-paradigm-shift s3://ethien-myfirst-bucket --recursive --include "*.html, *.js, *.css, *.jpg"
```
