package dao

import "database/sql"

// DB connection
type DB struct {
	DB *sql.DB
}
