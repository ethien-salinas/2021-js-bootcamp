package handler

import (
	"fmt"
	"net/http"
	"os"
	"socialnetwork-rest-api/data"
	"socialnetwork-rest-api/data/dao"
	"socialnetwork-rest-api/data/entity"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"golang.org/x/crypto/bcrypt"
)

func Login(c echo.Context) error {
	user := new(entity.User)
	if err := c.Bind(user); err != nil {
		return err
	}
	// Open DB connection
	conn, err := data.Open()
	if err != nil {
		return err
	}
	defer conn.Close()
	db := dao.DB{DB: conn}
	usr, err := db.FindUserByEmail(user.Email)
	if err != nil {
		log.Error(err)
		return echo.ErrUnauthorized
	}
	if err := bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(user.Password)); err != nil {
		log.Error(err)
		return echo.ErrUnauthorized
	}
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = usr.Name
	claims["lastname"] = usr.LastName
	claims["email"] = usr.Email
	claims["is_admin"] = usr.IsAdmin
	claims["exp"] = time.Now().Add(3 * time.Minute).Unix()
	jsonWebToken, err := token.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))
	if err != nil {
		log.Error(err)
		return err
	}
	fmt.Println(jsonWebToken)
	return c.JSON(http.StatusOK, map[string]string{
		"token": jsonWebToken,
	})

}
