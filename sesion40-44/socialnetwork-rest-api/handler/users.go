package handler

import (
	"log"
	"net/http"
	"socialnetwork-rest-api/data"
	"socialnetwork-rest-api/data/dao"
	"socialnetwork-rest-api/data/entity"
	"strconv"

	"github.com/labstack/echo/v4"
)

// SaveUser ...
func SaveUser(c echo.Context) error {
	user := new(entity.User)
	if err := c.Bind(user); err != nil {
		return err
	}
	// Open DB connection
	conn, err := data.Open()
	if err != nil {
		log.Fatal("Unable to connect to the DB, err: ", err)
	}
	defer conn.Close()
	db := dao.DB{DB: conn}
	db.InsertUser(*user)

	return c.JSONPretty(http.StatusCreated, user, " ")
}

// GetUser ...
func GetUser(c echo.Context) error {
	idStr := c.Param("id")
	if idStr != "" {
		id, err := strconv.Atoi(idStr)
		if err != nil {
			return err
		}
		// Open DB connection
		conn, err := data.Open()
		if err != nil {
			log.Fatal("Unable to connect to the DB, err: ", err)
		}
		defer conn.Close()
		db := dao.DB{DB: conn}
		user, err := db.FindUserByID(id)
		if err != nil {
			return err
		}
		return c.JSONPretty(http.StatusOK, map[string]interface{}{
			"id":       user.ID,
			"name":     user.Name,
			"lastname": user.LastName,
			"email":    user.Email,
			"isAdmin":  user.IsAdmin,
		}, " ")
	}
	return c.String(http.StatusOK, "User List")
}

// UpdateUser ...
func UpdateUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "User updated "+id)
}

// DeleteUser ...
func DeleteUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "User deleted "+id)
}
