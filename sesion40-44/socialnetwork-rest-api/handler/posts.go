package handler

import (
	"io/ioutil"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
)

// SavePost ...
func SavePost(c echo.Context) error {
	return c.JSONPretty(http.StatusCreated, "SavePost", " ")
}

// GetPost ...
func GetPost(c echo.Context) error {
	idStr := c.Param("id")
	if idStr != "" {
		return c.JSONPretty(http.StatusOK, map[string]interface{}{
			"id":   0,
			"name": "post",
		}, " ")
	}
	resp, err := http.Get(os.Getenv("TYPICODE_SERVER_URL") + "/posts")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	bodyString := string(body)

	return c.String(http.StatusCreated, bodyString)
}

// UpdatePost ...
func UpdatePost(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "UpdatePost "+id)
}

// DeletePost ...
func DeletePost(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "DeletePost "+id)
}
