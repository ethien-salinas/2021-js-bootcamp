package main

import (
	"log"
	"net/http"
	"socialnetwork-rest-api/handler"

	_ "github.com/mattn/go-sqlite3"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	// Load .env variables
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	// Routes
	e.GET("/", rootPath)
	e.POST("/login", handler.Login)

	// Groups
	postGpo := e.Group("/post")
	postGpo.POST("", handler.SavePost)
	postGpo.GET("", handler.GetPost)
	postGpo.GET("/:id", handler.GetPost)
	postGpo.PUT("/:id", handler.UpdatePost)
	postGpo.DELETE("/:id", handler.DeletePost)

	userGpo := e.Group("/user")
	userGpo.POST("", handler.SaveUser)
	userGpo.GET("", handler.GetUser)
	userGpo.GET("/:id", handler.GetUser)
	userGpo.PUT("/:id", handler.UpdateUser)
	userGpo.DELETE("/:id", handler.DeleteUser)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

// Handler
func rootPath(c echo.Context) error {
	return c.String(http.StatusOK, "Social Network REST API V1")
}
