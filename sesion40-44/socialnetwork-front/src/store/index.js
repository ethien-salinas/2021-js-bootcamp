// import { Promise } from 'core-js'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: {}
  },
  getters: {
    isLoggedIn: state => !!state.token
  },
  mutations: {
    auth_success(state, { token, user }) {
      console.log(token, user)
      state.status = 'success'
      state.token = token
      state.user = user
    },
    auth_err(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = 'logout'
      state.token = ''
      state.user = {}
    }
  },
  actions: {
    login({ commit }, loginData) {
      return new Promise((resolve, reject) => {
        try {
          fetch(`${process.env.VUE_APP_BASE_API_URL}/login`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(loginData)
          })
            .then(res => res.json())
            .then(data => {
              const token = data.token
              const payload = JSON.parse(atob(token.split('.')[1]))
              const user = {
                email: payload.email,
                name: payload.name,
                lastname: payload.lastname
              }
              console.log('user: ', user)

              localStorage.setItem('token', token)
              commit('auth_success', { token, user })
              resolve()
            })
        } catch (err) {
          commit('auth_err')
          localStorage.removeItem('token')
          reject(err)
        }
      })
    },

    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token')
        resolve()
      })
    }

  },
  modules: {
  }
})
