# Sesión 24-26
-------------------
#### Instalar dependencia browser-sync
Añadir en el archivo `package.json`, en la parte de scripts lo siguiente:
```
"scripts": {
    "start": "npx browser-sync src -w"
  }
```
`NOTA:` `npx` es una herramienta de cli que nos permite ejecutar paquetes de npm de forma sencilla sin necesidad de instalarlos, `-w` propiedad de browser-sync lo usamos para poder hacer reload automático de nuestro proyecto

#### Instalar react por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://cdn.jsdelivr.net/npm/react@17.0.1/umd/react.development.min.js"></script>
```

#### Instalar react-dom por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://cdn.jsdelivr.net/npm/react-dom@17.0.1/umd/react-dom.development.min.js"></script>
```

#### Instalar Babel por CDN
Agregar el siguiente script en la etiqueta head del html:
```
<script src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.12.12/babel.min.js"></script>
```

#### Iniciar proyecto react desde terminal
Con el comando `npx create-react-app` seguido del nombre del proyecto:
```
npx create-react-app my-first-react-project
```
Obtener react-developer-tools:
[Descarga la extensión para Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=es)

#### Algunos sitios de interes

  - [React](https://es.reactjs.org/)
  - [jsDelivr](https://www.jsdelivr.com/)
  - [Presentando JSX](https://es.reactjs.org/docs/introducing-jsx.html)
  - [PropTypes](https://es.reactjs.org/docs/typechecking-with-proptypes.html#proptypes)
  - [TypeScript](https://www.typescriptlang.org/)
  - [MDN Web Components](https://developer.mozilla.org/es/docs/Web/Web_Components)
  - [Web component](https://www.webcomponents.org/)
  - [Electron](https://www.electronjs.org/)
  - [React - state and lifecycle](https://es.reactjs.org/docs/state-and-lifecycle.html)
  - [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
  - [Using Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
  - [Using Fetch - Body](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#body)
  - [create-react-app](https://create-react-app.dev/docs/getting-started/)
  - [Measuring Performance](https://create-react-app.dev/docs/measuring-performance/)
  - [Modo Estricto](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Modo_estricto)
