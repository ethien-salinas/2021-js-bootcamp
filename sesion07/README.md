# Sesión 07
-------------------
#### Instalar dependencias y utilerías de webpack
```
$ npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge
```

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```

#### Webpack alternatives
- [Module - bundler](https://bestofjs.org/projects?tags=module)
- [Parcel](https://parceljs.org/)
- [Rollup.js ](https://rollupjs.org/guide/en/)
- [Neutrino](https://neutrinojs.org/)

#### Ejecutar proyecto en el navegador
`Nota:` En la version 5 de webpack tendremos que usar alguno de los siguientes scripts en nuestro package.json para correr el proyecto en los navegadores disponibles en nuestro computador (scripts para SO Windows).

`Google Chrome`
```
"scripts": {
    "start": "webpack serve --open 'Chrome.exe'"
  }
```

`Edge`
```
"scripts": {
    "start": "webpack serve --open 'msedge.exe'"
  }
```

`Firefox`
```
"scripts": {
    "start": "webpack serve --open 'firefox.exe'"
  }
```

`Brave`
```
"scripts": {
    "start": "webpack serve --open 'Brave.exe'"
  }
```
Podemos omitir el `.exe` si así lo deseamos.

#### Ejemplo para dispositivos Mac y Linux

`Mac en Google Chrome`
```
"scripts": {
    "start": "webpack serve --open 'Google Chrome'"
  }
```

`Linux en Google Chrome`
```
"scripts": {
    "start": "webpack serve --open 'google-chrome'"
  }
```

#### Algunos sitios de interes

  - [Webpack devtool](https://webpack.js.org/configuration/devtool/#devtool)
  - [Webpack mode](https://webpack.js.org/configuration/mode/)
  - [Webpack placeholders](https://webpack.js.org/loaders/file-loader/#placeholders)
  - [CSS Grid Layout](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Grid_Layout)
