# Sesión 02-04
-------------------
#### Crear cuenta en [GitLab](https://about.gitlab.com/)
#
#### Comandos básicos git

| Comandos | Descripción | Ejemplo |
| ------- | ----------- | ------- |
| git init | Crea un nuevo repositorio de git vacio o un proyecto existente | $ git init|
| git config --global user.name | Configura tu nombre de usuario de manera global, puedes omitir `--global` para hacerlo de manera local | $ git config --global user.name "John Doe"|
| git config --global user.email | Configura tu correo electronico de usuario de manera global, puedes omitir `--global` para hacerlo de manera local | $ git config --global user.email johndoe@example.com|
| git remote | Permite acceder a la conexión entre el repositorio y maquina local | $ git remote |
| git remote add | Agrega una conexión a un servicio de alojamiento de código como un servidor interno (GitLab, GitHub, etc...). En el ejemplo, `origin` será el nombre asociado al repositorio en conexión  | $ git remote add origin git@gitlab.com:ethien-salinas/2021-js-bootcamp.git |
| git remote -v | Visualiza la conexión a un servicio de alojamiento de código como un servidor interno (GitLab, GitHub, etc...) | $ git remote -v |
| git clone | Puede clonar el repositorio remoto para crear una copia local en su sistema | $ git clone git@gitlab.com:ethien-salinas/2021-js-bootcamp.git |
| git status | Muestra el estado del repositorio permitiendo ver los archivos y cambios rastreados y no rastreados | $ git status |
| git add | Se utiliza para agregar contenido actual del árbol de trabajo al área de preparación para la próxima confirmación  | $ git add sesion02-04/ |
| git commit | Se utiliza para registrar los cambios en el repositorio, podemos utilizar -m para añadir un mensaje | $ git commit -m "[add] sesion02-04" |
| git push | Es el acto de transferencia de confirmaciones desde su repositorio local a un repositorio remoto | $ git push origin master |
| git pull |  Obtiene y combina los cambios del servidor remoto a su directorio de trabajo actual | $ git pull |  
| git fetch |  Se usa para obtener una nueva vista de todas las cosas que sucedieron en un repositorio remoto, por ejemplo extraer las actualizaciones de las ramas de seguimiento remoto sin afectar nuestro trabajo actual | $ git fetch |
| git log | Mostrará el historial de todas las confirmaciones | $ git log |
| git diff | Se utiliza para mostrar cambios entre confirmaciones, archivos, ramas, etc | $ git diff index.js |
| git restore | Vuelve en algun punto en la linea del tiempo, por ejemplo a un commit anterior | $ git restore 6a0531f |
| git checkout | Se utiliza para moverse entre ramas y commit | $ git checkout master |
| git branch | Crea una rama nueva | $ git branch hello-world |
| git merge | Fusiona una rama con tu rama actual | $ git merge master |

#### Intsalar dependencia Jest
Para instalar jest en modo desarrollo ejecutamos en la terminal el siguiente comando:
```
$ npm i -D jest
```

#### Intsalar dependencia babel-jest y @babel/preset-env
Para instalar babel en modo desarrollo ejecutamos en la terminal el siguiente comando:
```
$ npm i -D babel-jest @babel/preset-env
```
`NOTA:` En el archivo `package.json` añadimos en la parte de scripts lo siguiente:
``` 
"jest": {
    "transform": {
      "^.+\\.[t|j]sx?$": "babel-jest"
    }
  }
```
Posterior añadiremos en la raiz del proyecto un archivo babel.config.json para tener completo nuestro ambiente de pruebas unitarias, en el archivo añadir lo siguiente:
```
{
  "presets": [
    "@babel/preset-env"
  ]
}
```

#### Algunos sitios de interes

  - [Jest](https://jestjs.io/)
  - [Jest Using Matchers](https://jestjs.io/docs/en/using-matchers)
  - [gitignore](https://github.com/github/gitignore)
  - [Can I Use](https://caniuse.com/)
  - [PlaceHolder](https://placeholder.com/)
  - [Cup Cake Ipsum](http://www.cupcakeipsum.com/)
  - [String Methods](https://www.w3schools.com/js/js_string_methods.asp)
  - [String Methods MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String#Instance_methods)
  - [Falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)
  - [Truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy)
  - [Behavior Parameterization](https://blog.indrek.io/articles/java-8-behavior-parameterization/)
  - [First-class Function](https://developer.mozilla.org/en-US/docs/Glossary/First-class_Function)
  - [Arrays](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array)
  - [Strings](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String)