import { getProductHTML, strStartsWith, templateString } from "../strings";
import { getProductHTMLTestcases, strStartsWithTestcases, templateStringTestcases } from "./testcases/strings-testcases";

templateStringTestcases.forEach(testElement =>
  test(`template string test with: ${testElement.entrada.name}`, () => {
    const entrada = templateString(testElement.entrada)
    const salida = testElement.salida
    expect(entrada).toBe(salida)
  })
)

getProductHTMLTestcases.forEach(testElement =>
  test(`getProductHTML test with: ${testElement.entrada.name}`, () => {
    const entrada = getProductHTML(testElement.entrada)
    const salida = testElement.salida
    expect(entrada).toBe(salida)
  })
)

strStartsWithTestcases.forEach(testElement =>
  test(`strStartsWith test with: ${testElement.entrada.name}`, () => {
    const entrada = strStartsWith(testElement.entrada.desc, testElement.entrada.toFind)
    const salida = testElement.salida
    expect(entrada).toBe(salida)
  })
)
