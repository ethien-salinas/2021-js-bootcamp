// Array.from
// Array.of
// Array.prototype.fill
// Array.prototype.includes
// Array.prototype.find

test('Array.from test cases', () => {
  // console.log(Array.from(1, 2, 3)); //esta falla
  console.log(Array.from([1, 2, 3]));
  console.log(Array.from([1, 2, 3], n => n * n)); //este es su valor
  return
})
test('Array.of test cases', () => {
  console.log(Array.of(1, 2, 3, 'cadena'));
  console.log(Array.of([1, 2, 3]));
  console.log(Array.of(1, 2, 3, n => n * n)); //este es su valor
  return
})
test('Array.fill test cases', () => {
  console.log(new Array(9).fill(1, 2, 3));
  console.log(new Array(9).fill(1, 2, 4));
  console.log(new Array(9).fill(1, 2, 5));
  console.log(new Array(9).fill('x', 3, 6));
  return
})
test('Array.find test cases', () => {
  // devuelve solo al primero que cumpla el predicado
  console.log([1, 2, 3, 4, 5, 6].find(n => n % 2));
  console.log([1, 2, 3, 4, 5, 6].find(n => n > 9)); //undefined
  return
})
test('Array.filter test cases', () => {
  // devuelve todos los que cumplen con el predicado
  console.log([1, 2, 3, 4, 5, 6].filter(n => n % 2));
  return
})