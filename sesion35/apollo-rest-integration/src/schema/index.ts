import { gql } from "apollo-server"
import { Book } from "./model/Book"
import { Post } from "./model/Post"
import { Mutation } from "./root/Mutations"
import { Query } from "./root/Query"

export const typeDefs = gql`
  ${Book}
  ${Post}
  ${Query}
  ${Mutation}
`