# Sesión 35
-------------------
#### Instalar dependencias
```
npm i apollo-datasource-rest apollo-server graphql
```

#### Instalar dependencias de desarrollo
```
npm i -D typescript ts-node @types/node
```

#### Algunos sitios de interes

  - [jsonplaceholder](https://jsonplaceholder.typicode.com/posts)
  - [Apollo REST Data Source](https://github.com/apollographql/apollo-server/tree/main/packages/apollo-datasource-rest)
  - [Apollo Resolvers](https://www.apollographql.com/docs/apollo-server/data/resolvers/)
