# Sesión 37
-------------------
#### Instalar dependencias
```
npm i apollo-datasource apollo-server graphql reflect-metadata sqlite3 typeorm
```

#### Instalar dependencias de cifrado
```
npm i bcrypt dotenv jsonwebtoken
```

#### Instalar dependencias de desarrollo
```
npm i -D typescript ts-node @types/node @types/bcrypt @types/jsonwebtoken
```