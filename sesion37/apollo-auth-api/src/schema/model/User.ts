import { gql } from 'apollo-server'

export const User = gql`
  type User {
    id: Int
    name: String
    lastname: String
    email: String
    isAdmin: Boolean
  }
  input UserInput {
    name: String
    lastname: String
    email: String
    password: String
    isAdmin: Boolean
  }
`
