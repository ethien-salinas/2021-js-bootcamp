import { gql } from 'apollo-server'

export const Mutation = gql`

  type Mutation {
    signUp(input: UserInput): User
    saveBook(input: BookInput): Book
    updateBook(id: Int!, input: BookInput): Book
    deleteBook(id: Int!): Book
  }

`
