import { DataSource } from 'apollo-datasource'
import { ApolloError, AuthenticationError } from 'apollo-server'
import { compare } from 'bcrypt'
import { sign, verify } from 'jsonwebtoken'
import { Connection, Repository } from 'typeorm'
import { User } from '../db/entity/User'

export class AuthAPI extends DataSource {

  private userRepository: Repository<User>

  constructor(connection: Connection) {
    super()
    this.userRepository = connection.getRepository(User)
  }

  async getToken({ email, password }): Promise<string> {
    const user = await this.userRepository.findOne({ where: { email } })
    if (user && await compare(password, user.password)) {
      const tokenData = {
        fullName: user.name + ' ' + user.lastname,
        email,
        isAdmin: user.isAdmin
      }
      return sign(tokenData, process.env.JWT_SECRET, { expiresIn: 180 })
    } else {
      throw new AuthenticationError('Invalid credentials')
    }
  }

  verifyToken(token: string): boolean {
    let isValid: boolean = false
    if (!token) throw new ApolloError('missing token')
    const decoded = verify(token, process.env.JWT_SECRET)
    return decoded ? !isValid : isValid
  }

}