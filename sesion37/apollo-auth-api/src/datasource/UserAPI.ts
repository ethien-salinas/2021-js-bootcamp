import { DataSource } from 'apollo-datasource'
import { hash } from 'bcrypt'
import { Connection, Repository } from 'typeorm'
import { User } from '../db/entity/User'
import { IUser } from '../types'

export class UserAPI extends DataSource {

  private repository: Repository<User>
  private SALT_ROUNDS = 9

  constructor(connection: Connection) {
    super()
    this.repository = connection.getRepository(User)
  }

  async saveUser(user: IUser): Promise<IUser> {
    user.password = await hash(user.password, this.SALT_ROUNDS)
    return await this.repository.save(user)
  }


}