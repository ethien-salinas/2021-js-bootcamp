INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language, total_pages) VALUES ('Karaoke Capitalism', 'Jonas Ridderstråle, Jonas Ridderstrale, Kjell Nordström', 'Pearson Education', '2736874762', '9780273687474', 'Business & Economics', 2004, 'en', 311);

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Installing & Repairing Plumbing Fixtures', 'Peter Hemp', 'Taunton', '1561580759', '9781561580750', 'House & Home', 1994, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Uproar in the House', 'Anthony Marriott, Alistair Foot', 'London : Evans Plays', '237749084', '9780237749088', 'English drama', 1973, 'en');

INSERT INTO book (title, author, publisher, category, year, language, total_pages) VALUES ('Shipping Law in China', 'John Mo', 'Sweet & Maxwell', 'Law', 1999, 'en', 488);

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, year, language) VALUES ("The Sire de Maldroit's Door", 'Robert Louis Stevenson', 'Dramatic Publishing', '1583425012', '9781583425015', 1986, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Spartan Women', 'Sarah B. Pomeroy', 'Oxford University Press, USA', '195130677', '9780195130676', 'History', 2002, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Interactive and Sculptural Printmaking in the Renaissance', 'Suzanne Kathleen Karr Schmidt', "Brill's Studies in Intellectua", '9004340130', '9789004340138', 'History', 2018, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Essential virtual reality fast', 'John A. Vince', 'Springer-Verlag New York Inc', '1852330120', '9781852330125', 'Computers', 1998, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, language) VALUES ('The Importance of Motivation in an Educational Environment', 'Gazala Bhoje', 'Lulu.com', '1329128702', '9781329128705', 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, language) VALUES ('THE REBEL KING', 'Harlequin Books S.A.', 'Harlequin / SB Creative', '4596687994', '9784596687999', 'Comics & Graphic Novels','en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('The Souls of Poor Folk', 'Charles Lattimore Howard', 'University Press of America', '761838562', '9780761838562', 'Social Science', 2008, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Lighting', 'Scala Quin', 'New Holland Pub Limited', '1780090609', '9781780090603', 'Antiques & Collectibles', 2012, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Abba Zappa', 'Gijsbert Hanekroot', 'Veenman Pub', '908690128X', '9789086901289', 'Music', 2008, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ("It's Your Move, Jennifer", 'Jane Sorenson', 'Standard Pub', '872397726', '9780872397729', 'Christian life', 1984, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Fly Fishing', 'John Merwin, Ron Hildebrand', 'W. W. Norton & Company', '393314766', '9780393314762', 'Sports & Recreation', 1996, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Living with Depression', 'Facts On File, Incorporated', 'Infobase Publishing', '1438121075', '9781438121079', 'Depression in adolescence', 2007, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('A Loose Canon', 'Brian J. Coman', 'Connor Court Publishing Pty Ltd', '980293626', '9780980293623', 'Australian essays', 2007, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Tolerance', 'Hans Oberdiek', 'Rowman & Littlefield', '847687864', '9780847687862', 'Political Science', 2001, 'en');

INSERT INTO book (title, author, publisher, isbn_10, isbn_13, category, year, language) VALUES ('Depression', 'Tony Bates', 'NewLeaf', '717128601', '9780717128600', 'Depression, Mental', 1999, 'en');

INSERT INTO book (title, author, publisher, category, year, language, total_pages) VALUES ('The deeper meaning of liff', 'Douglas Adams, John Lloyd, Bert Kitchen', 'Harmony', 'Humor', 1990, 'en', 156);