export const resolvers = {
  Query: {
    signIn: (_, { email, password }, { dataSources }) =>
      dataSources.authAPI.getToken({ email, password }),

    getAllBooks: (_, __, { dataSources, token }) =>
      dataSources.authAPI.verifyToken(token)
      && dataSources.booksAPI.getBooks(),

    getBook: (_, { id }, { dataSources, token }) =>
      dataSources.authAPI.verifyToken(token)
      && dataSources.booksAPI.getBook(id),
  },

  Mutation: {
    signUp: (_, { input }, { dataSources }) =>
      dataSources.usersAPI.saveUser({ ...input }),

    saveBook: (_, { input }, { dataSources, token }) =>
      dataSources.authAPI.verifyToken(token)
      && dataSources.booksAPI.saveBook({ ...input }),

    updateBook: (_, { id, input }, { dataSources, token }) =>
      dataSources.authAPI.verifyToken(token)
      && dataSources.booksAPI.updateBook({ id, ...input }),

    deleteBook: (_, { id }, { dataSources, token }) =>
      dataSources.authAPI.verifyToken(token)
      && dataSources.booksAPI.deleteBook(id),
  }
}