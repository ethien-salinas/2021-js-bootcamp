require('dotenv').config()
import { ApolloServer } from 'apollo-server'
import { getDBConnection } from './db'
import { typeDefs } from './schema'
import { resolvers } from './resolver'
import { BookAPI } from './datasource/BookAPI'
import { Connection } from 'typeorm'
import { UserAPI } from './datasource/UserAPI'
import { AuthAPI } from './datasource/AuthAPI'

let connection: Connection
(async () => {
  connection = await getDBConnection()
})()

new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      booksAPI: new BookAPI(connection),
      usersAPI: new UserAPI(connection),
      authAPI: new AuthAPI(connection),
    }
  },
  context: async ({ req }) => {
    return {
      token: req.headers.authorization || ''
    }
  }
})
  .listen()
  .then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  })