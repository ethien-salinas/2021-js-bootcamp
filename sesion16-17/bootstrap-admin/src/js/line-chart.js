const data = {
  labels: ['Running', 'Swimming', 'Eating', 'Cycling'],
  datasets: [{
    label: 'tabla de control',
    data: [20, 15, 6, 9],
    backgroundColor: 'rgba(242, 5, 5, 0.5)'
  }]
}
const options = {
  scale: {
    angleLines: {
      display: false
    },
    suggestedMin: 50,
    suggestedMax: 100
  }
}

const ctx = document.querySelector('#lineChart')
const radarChart = new Chart(ctx, {
  type: 'radar',
  data,
  options
});