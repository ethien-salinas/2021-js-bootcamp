# Sesión 33
-------------------
#### Instalar dependencias 
```
npm i express dotenv bcrypt jsonwebtoken sqlite3 typeorm reflect-metadata
```

#### Instalar dependencias de desarrollo
```
npm i -D typescript ts-node @types/node @types/express @types/jsonwebtoken @types/bcrypt
```

#### Algunos sitios de interes

  - [HTTP Header Authorization](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Authorization)
  - [Bearer Authentication](https://swagger.io/docs/specification/authentication/bearer-authentication/)
