require('dotenv').config()
import express, { Request, Response } from "express";
import { authRouter } from "./routes/auth";
import { userRouter } from "./routes/user";
import { printHeaders } from "./middleware/header";
import { authorization } from "./middleware/authorization";

const app = express()
const port: number = 3000

app.use(express.json())

app.get('/', printHeaders, (req: Request, res: Response) => {
  res.send('Express Auth API V1')
})

app.use('/auth', authRouter)
app.use('/user', authorization, userRouter)

app.listen(port, () => {
  console.log(`API listening at http://localhost:${port}`)
})
