import express, { Request, Response } from 'express'
import { Connection } from 'typeorm'
import { UserAPI } from '../db/api/UserAPI'

export const userRouter = express.Router()

// let connection: Connection
// let userAPI: UserAPI

userRouter.post('/', async (req: Request, res: Response) => {
  res.send(`insert user ${JSON.stringify(req.body)}`,)
})
userRouter.get('/', async (req: Request, res: Response) => {
  res.send('get users')
})
userRouter.get('/:id', async (req: Request, res: Response) => {
  res.send(`get user ${req.params.id}`)
})
userRouter.put('/:id', async (req: Request, res: Response) => {
  res.send(`update user ${req.params.id}`)
})
userRouter.delete('/:id', async (req: Request, res: Response) => {
  res.send(`delete user ${req.params.id}`)
})
