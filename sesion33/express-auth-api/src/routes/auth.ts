import express, { Request, Response } from 'express'
import { IUser } from "../model/IUser";
import bcrypt from "bcrypt";
import { sign } from "jsonwebtoken";
import { Connection } from 'typeorm';
import { UserAPI } from '../db/api/UserAPI';
import { getDBConnection } from '../db';

export const authRouter = express.Router()

let connection: Connection
let userAPI: UserAPI

authRouter.post('/login', async (req: Request, res: Response) => {
  const { email, password } = req.body
  connection = await getDBConnection()
  userAPI = new UserAPI(connection)
  const user = await userAPI.getUserByEmail(email)
  console.log(user);
  connection.close()
  if (user && await bcrypt.compare(password, user.password)) {
    const tokenData = {
      name: user.name,
      email: user.email
    }
    const token = sign(tokenData, process.env.JWT_SECRET, { expiresIn: 180 })
    res.json({ token })
  } else {
    res.status(401).json({ error: 'Unauthorized' })
  }
})

authRouter.post('/register', async (req: Request, res: Response) => {
  const { name, email, password } = req.body
  const user: IUser = {
    name,
    email,
    password
  }
  user.password = await bcrypt.hash(password, 9)
  connection = await getDBConnection()
  userAPI = new UserAPI(connection)
  const result = await userAPI.saveUser(user)
  connection.close()
  res.send(result)
})